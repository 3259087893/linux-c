#ifndef __SPEECHMANAGER_H__
#define __SPEECHMANAGER_H__
#include <iostream>
#include <vector>
#include <map>
#include "speaker.h"
using namespace std;

//功能：  提供菜单界面与用户交互
//       对演讲比赛流程进行控制
//       与文件读写交互
class SpeechManager
{
public:
    //构造函数
    SpeechManager();
    //菜单显示
    void show_Menu(void);
    void exitSystem();




    //析构函数
    ~SpeechManager();

    //初始化容器和属性
    void initspeech();
    //创建选手
    void speakercreate();
    void startspeech(void);

//成员属性

    vector<int>v1;              //存放12个比赛选手的容器
    vector<int>v2;              //存放第一轮晋级选手
    vector<int>v3;              //存放胜利前三名
    map<int,Speaker>m_Speaker;  //存放具体选手编号及选手
    int m_index;                 //记录比赛轮数

};




#endif