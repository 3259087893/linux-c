#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include "test.h"

Node* g_pStudentList=NULL;		

int main(void)
{
	while (1)
	{

		Welcome();
		char ch = getchar();
		switch (ch)
		{
		case '1':
			InputStudent();
			break;
		case '2':
			PrintStudent();
			break;
		case '3':
			break;
		case '4':
			break;
		case '5':
			break;
		case '6':
			printf("Bye �� Bye ~~~\n");
			return 0;
		}

	}
	return 0;
}

void Welcome()
{
	printf("**************************************************\n");
	printf("*                                                *\n");
	printf("*          		学生管理系统          		  	  *\n");
	printf("*                                                *\n");
	printf("*              	输入命令               			  *\n");
	printf("*                 1.增          				 *\n");
	printf("*                 2.删           		    	 *\n");
	printf("*                 3.改          	   			 *\n");
	printf("*                 4.查              			 *\n");
	printf("*                 5.初始化               		 *\n");
	printf("*                 6.退出                		 *\n");
	printf("*                                                *\n");
	printf("**************************************************\n");
}

void InputStudent()
{
	Node* pNewNode = (Node*)malloc(sizeof(Node));
	pNewNode->next = NULL;

	printf("������ѧ��������\n");
	scanf("%s", pNewNode->stu.name);
	printf("������ѧ����ѧ�ţ�\n");
	scanf("%d", &pNewNode->stu.stunum);
	printf("������ѧ�����Ա�:\n");
	scanf("%s", pNewNode->stu.sex);
	printf("������ѧ�������ĳɼ�:\n");
	scanf("%f", &pNewNode->stu.chinese);
	printf("������ѧ������ѧ�ɼ�:\n");
	scanf("%f", &pNewNode->stu.math);
	printf("������ѧ����Ӣ��ɼ�:\n");
	scanf("%f", &pNewNode->stu.english);


	if (g_pStudentList == NULL)
	{
		g_pStudentList = pNewNode;
	}
	else
	{
		pNewNode->next = g_pStudentList;
		g_pStudentList = pNewNode;
	}
	
	printf("¼��ѧ����Ϣ�ɹ�.\n");
	system("sleep 3");
	system("clear");
}


void PrintStudent()
{
	system("cls");

	printf("**********************************************************\n");
	printf("*                                                        *\n");
	printf("*              ��ӭʹ�ø�Уѧ���ɼ�����ϵͳ              *\n");
	printf("*                                                        *\n");
	printf("* ѧ��\t* ����\t\t* �Ա�\t* ����\t* ��ѧ\t* Ӣ��   *\n");
	printf("**********************************************************\n");

	Node* p = g_pStudentList;
	while (p)
	{
		printf("* %d\t* %s\t* %s\t* %.1f\t* %.1f\t* %.1f\t*\n", 
			p->stu.stunum,
			p->stu.name,
			p->stu.sex,
			p->stu.chinese,
			p->stu.math,
			p->stu.english
			);
		p = p->next;
	}

	printf("**********************************************************\n");
	system("sleep 3");
	system("clear");
}
